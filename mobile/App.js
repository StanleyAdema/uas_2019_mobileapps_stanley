import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button} from 'react-native';
import Driver from "./Screens/Driver";
import Passenger from "./Screens/Passenger";

export default class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            isDriver: false,
            isPassenger: false,
        };
    }

    render() {
        if (this.state.isDriver) {
            return <Driver />
        }
        if (this.state.isPassenger) {
            return <Passenger/>
        }
        return (
            <View style={styles.container}>
                <Text
                    style={{margin: 10}}
                >
                    Choose your Hero
                </Text>
                <View
                    style={{margin: 10}}
                >
                    <Button
                        onPress={() => this.setState({ isPassenger: true})}
                        title="Passenger"
                    />
                </View>
                <View
                    style={{margin: 10}}
                >
                    <Button
                        onPress={() => this.setState({ isDriver: true})}
                        title="Driver"
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 50,
        justifyContent: "center",
        alignItems: "center"
    }
});
